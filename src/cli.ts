/* eslint-disable unicorn/no-null */
/*
 * Copyright (C) 2018-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {Command} from 'commander';
import {readFileSync, writeFileSync} from 'fs';
import got from 'got';
import path from 'path';
import {exit} from 'process';
import {generateTemplate} from './mapping';
import {getProjectReflection} from './project-reflection';
import {ElasticsearchTemplateCollection} from './types/mapping';

// handle unhandled promise rejections
process.on('unhandledRejection', async (reason: unknown) => {
  if (reason instanceof Error) {
    await Logger.error(reason.message);
    Logger.info(reason.stack);
  }
  process.exit(1);
});

const commander = new Command('openstapps-core-tools');

// eslint-disable-next-line unicorn/prefer-module
commander.version(JSON.parse(readFileSync(path.resolve(__dirname, '..', 'package.json')).toString()).version);

commander
  .command('mapping <relativeSrcPath>')
  .option('-m, --mappingPath <relativeMappingPath>', 'Mapping Path')
  .option('-i, --ignoredTags <ignoredTags>', 'Ignored Tags (comma-separated)')
  .option('-a, --aggPath <relativeAggregationPath>', 'Aggregations Path')
  .option('-e, --errorPath <relativeErrorPath>', 'Error Path')
  .action(async (relativeSourcePath, options) => {
    // get absolute paths
    const sourcePath = path.resolve(relativeSourcePath);

    let ignoredTagsList: string[] = [];
    if (typeof options.ignoredTags === 'string') {
      ignoredTagsList = options.ignoredTags.split(',');
    }

    // get project reflection
    const projectReflection = getProjectReflection(sourcePath);

    const result = generateTemplate(projectReflection, ignoredTagsList, true);
    if (result.errors.length > 0) {
      await Logger.error('Mapping generated with errors!');
    } else {
      Logger.ok('Mapping generated without errors!');
    }

    // write documentation to file
    if (typeof options.aggPath !== 'undefined') {
      const aggPath = path.resolve(options.aggPath);
      // tslint:disable-next-line:no-magic-numbers
      writeFileSync(aggPath, JSON.stringify(result.aggregations, null, 2));
      Logger.ok(`Elasticsearch aggregations written to ${aggPath}.`);
    }
    if (typeof options.mappingPath !== 'undefined') {
      const mappingPath = path.resolve(options.mappingPath);
      // tslint:disable-next-line:no-magic-numbers
      writeFileSync(mappingPath, JSON.stringify(result.mappings, null, 2));
      Logger.ok(`Elasticsearch mappings written to ${mappingPath}.`);
    }
    if (typeof options.errorPath !== 'undefined') {
      const errorPath = path.resolve(options.errorPath);
      // tslint:disable-next-line:no-magic-numbers
      writeFileSync(errorPath, JSON.stringify(result.errors, null, 2));
      Logger.ok(`Mapping errors written to ${errorPath}.`);
    } else if (result.errors.length > 0) {
      for (const error of result.errors) {
        await Logger.error(error);
      }

      throw new Error('Mapping generation failed');
    }
  });

commander
  .command('put-es-templates <srcPath> <esAddress> [ignoredTags]')
  .action(async (relativeSourcePath, esAddress) => {
    // get absolute paths
    const sourcePath = path.resolve(relativeSourcePath);

    // get project reflection
    // eslint-disable-next-line @typescript-eslint/no-var-requires,unicorn/prefer-module
    const templates = require(sourcePath) as ElasticsearchTemplateCollection;

    for (const template in templates) {
      if (!templates.hasOwnProperty(template)) {
        continue;
      }

      const response = await got.put(`${esAddress}_template/${template}`, {
        json: templates[template],
      });

      const HTTP_STATUS_OK = 200;
      if (response.statusCode !== HTTP_STATUS_OK) {
        await Logger.error(
          `Template for "${template}" failed in Elasticsearch:\n${JSON.stringify(response.body)}`,
        );
        exit(-1);
      }
    }

    Logger.ok(`Templates accepted by Elasticsearch.`);
  });

commander.parse(process.argv);
