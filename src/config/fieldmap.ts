/*
 * Copyright (C) 2019-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {MappingProperty} from '@elastic/elasticsearch/lib/api/types';
import {ElasticsearchFieldmap, SimpleType} from '../types/mapping';

const ducetSort = {
  type: 'icu_collation_keyword',
  language: 'de',
  country: 'DE',
  variant: '@collation=phonebook',
};

const keyword: MappingProperty['type'] = 'keyword';

export const fieldmap: ElasticsearchFieldmap = {
  aggregatable: {
    default: {
      raw: {
        ignore_above: 10_000,
        type: keyword,
      },
    },
    ignore: ['global'],
  },
  sortable: {
    default: {
      sort: ducetSort,
    },
    ducet: {
      sort: ducetSort,
    },
    ignore: ['price'],
  },
};

export const filterableTagName = 'filterable';

export const filterableMap: Record<string, SimpleType> = {
  date: 'keyword',
  keyword: 'keyword',
  text: 'keyword',
  integer: 'integer',
};
