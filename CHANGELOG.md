# [0.6.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.5.0...v0.6.0) (2023-04-28)



# [0.5.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.4.0...v0.5.0) (2023-04-27)



# [0.4.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.3.0...v0.4.0) (2023-01-12)



# [0.3.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.2.0...v0.3.0) (2022-08-17)



# [0.2.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.1.0...v0.2.0) (2022-06-27)


### Bug Fixes

* marked version with audit issue ([7534744](https://gitlab.com/openstapps/es-mapping-generator/commit/7534744294ec27f1ce3f4bf65424d717b33dd889))



# [0.1.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.0.4...v0.1.0) (2022-05-27)



## [0.0.4](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.0.3...v0.0.4) (2021-12-16)


### Bug Fixes

* filter tags override inherited type tags ([f0401e1](https://gitlab.com/openstapps/es-mapping-generator/commit/f0401e1889f12860d4eaa2dc5a1ee1b08f20ba31))



## [0.0.3](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.0.2...v0.0.3) (2021-08-05)


### Bug Fixes

* put es template parsing ([9a88129](https://gitlab.com/openstapps/es-mapping-generator/commit/9a881299dc9d0a6d59b370340880a10bd8b1e2c5))



## [0.0.2](https://gitlab.com/openstapps/es-mapping-generator/compare/28334b800c014a9e0b6e980b5365af6fdc1d8950...v0.0.2) (2021-08-05)


### Bug Fixes

* fix unwanted crash when not supplying an error path ([28334b8](https://gitlab.com/openstapps/es-mapping-generator/commit/28334b800c014a9e0b6e980b5365af6fdc1d8950))



