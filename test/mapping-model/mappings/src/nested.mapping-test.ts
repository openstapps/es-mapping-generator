/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface Nested {
  foo: {
    depth1_1: {
      depth2_1: {
        depth3_1: number;
        /**
         * @keyword
         */
        depth3_2: string;
      };
      depth2_2: boolean;
    };
  };

  type: ThingType.Nested;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Nested properties should work',
  name: ThingType.Nested,
  map: {
    maps: {
      foo: {
        dynamic: 'strict',
        properties: {
          depth1_1: {
            dynamic: 'strict',
            properties: {
              depth2_1: {
                dynamic: 'strict',
                properties: {
                  depth3_1: {
                    type: 'integer',
                  },
                  depth3_2: {
                    type: 'keyword',
                  },
                },
              },
              depth2_2: {
                type: 'boolean',
              },
            },
          },
        },
      },
    },
  },
};
