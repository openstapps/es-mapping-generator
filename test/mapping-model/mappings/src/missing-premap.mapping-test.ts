/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {MISSING_PREMAP} from '../../../../lib/config/typemap';
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface MissingPremap {
  // it really doesn't matter what we use here, as long as it is an external dependency
  // if you get an error here because you removed a dependency, feel free to change it around
  // to your heart's content
  foo: HTMLAllCollection;

  type: ThingType.MissingPremap;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Missing premap should cause an error',
  name: ThingType.MissingPremap,
  map: {
    maps: {
      foo: {
        type: MISSING_PREMAP,
      },
    },
  },
  err: [`At "${ThingType.MissingPremap}::foo" for external type "HTMLAllCollection": Missing pre-map`],
};
