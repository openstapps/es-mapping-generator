/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {TYPE_CONFLICT} from '../../../../lib/config/typemap';
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface DoubleTypeConflict {
  /**
   * @keyword
   * @text
   */
  stringDoubleTypeConflict: string;

  /**
   * @integer
   * @float
   */
  numberDoubleTypeConflict: number;

  type: ThingType.DoubleTypeConflict;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Double type annotations should cause an error',
  name: ThingType.DoubleTypeConflict,
  map: {
    maps: {
      stringDoubleTypeConflict: {
        type: TYPE_CONFLICT,
      },
      numberDoubleTypeConflict: {
        type: TYPE_CONFLICT,
      },
    },
  },
  err: [
    `At "${ThingType.DoubleTypeConflict}::stringDoubleTypeConflict" for type "string": Type conflict; "keyword" would override "text"`,
    `At "${ThingType.DoubleTypeConflict}::numberDoubleTypeConflict" for type "number": Type conflict; "integer" would override "float"`,
  ],
};
