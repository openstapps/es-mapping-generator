/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface DoubleTypeConflict {
  /**
   * @keyword
   */
  keywordNumber: number;

  /**
   * @text
   */
  textNumber: number;

  /**
   * @integer
   */
  integerString: string;

  /**
   * @float
   */
  floatString: string;

  type: ThingType.IncompatibleType;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Incompatible type annotations should cause an error and use defaults',
  name: ThingType.IncompatibleType,
  map: {
    maps: {
      keywordNumber: {
        type: 'integer',
      },
      textNumber: {
        type: 'integer',
      },
      integerString: {
        type: 'text',
      },
      floatString: {
        type: 'text',
      },
    },
  },
  err: [
    `At "${ThingType.IncompatibleType}::keywordNumber" for tag "keyword": Not implemented tag`,
    `At "${ThingType.IncompatibleType}::textNumber" for tag "text": Not implemented tag`,
    `At "${ThingType.IncompatibleType}::floatString" for tag "float": Not implemented tag`,
    `At "${ThingType.IncompatibleType}::integerString" for tag "integer": Not implemented tag`,
  ],
};
